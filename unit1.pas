unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  LCLType,Classes, crt, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Shape1: TShape;
    Shape1_1: TShape;
    Shape1_2: TShape;
    Shape1_3: TShape;
    Shape1_4: TShape;
    Shape1_5: TShape;
    Shape2_1: TShape;
    Shape2_2: TShape;
    Shape2_3: TShape;
    Shape2_4: TShape;
    Shape2_5: TShape;
    Shape3_1: TShape;
    Shape3_2: TShape;
    Shape3_3: TShape;
    Shape3_4: TShape;
    Shape3_5: TShape;
    Shape4_1: TShape;
    Shape4_2: TShape;
    Shape4_3: TShape;
    Shape4_4: TShape;
    Shape4_5: TShape;
    Shape5_1: TShape;
    Shape5_2: TShape;
    Shape5_3: TShape;
    Shape5_4: TShape;
    Shape5_5: TShape;
    StaticText1: TStaticText;
    Timer1: TTimer;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button5KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Shape1_1ChangeBounds(Sender: TObject);
    procedure StaticText1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);





  private

  public



  end;
Type
Vector = array [1..5] of int64;
vec2=array[1..2] of int64;
Vectorsh= array [1..5] of tshape;
matrixsh= array [1..5] of Vectorsh;
Matrix= array [1..5] of vector;
matrix3= array [1..5] of matrix;
Var m: matrix;
var headpos:vec2;
var newstring:string;
Var m3: matrix3;
var msh:matrixsh;
var i:int64;var j:int64;var k:int64;
var initiator1:int64;
var snakelength:int64;
var snakedirection:string;
var
  Form1: TForm1;
  n:int64;
  but1:TButton;

implementation

{$R *.lfm}

{ TForm1 }

procedure game_over(label_i:Tlabel;timer_i:ttimer);
begin
   label_i.caption:='Game over! Your score='+snakelength.tostring;
   Timer_i.enabled:=false;
end;

{procedure randomfill(m3:matrix3);
   var i:int64;
   var j:int64;
   var k:int64;

begin
  k:=1;
  Randomize;
  while k=1 do begin
  i:=random(5)+1;
  j:=random(5)+1;
  if m3[i,j,1]=0 then
  begin
  m3[i,j,1]:=-1;
  k:=0;
  end;
  end;
end; }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
   Randomize;



   shape1.Top:=shape1.Top-1;
     label1.Caption:=m3[1,1,1].tostring;
     if initiator1=0 then
     begin
       if k>0 then
       k:=k-1
       else
       initiator1:=1;
       i:=5;
       while i>0 do begin
         j:=5;
         while j>0 do begin
              m3[i,j,1]:=0;
            j:=j-1;
         end;
         i:=i-1;
       end;
       m3[1,5,1]:=-1;
       //m3[1,3,1]:=3;
       headpos[1]:=1;
       headpos[2]:=3;
       snakelength:=3;
       snakedirection:='right';
       msh[1,1]:=shape1_1;
       msh[1,2]:=shape1_2;
       msh[1,3]:=shape1_3;
       msh[1,4]:=shape1_4;
       msh[1,5]:=shape1_5;
       msh[2,1]:=shape2_1;
       msh[2,2]:=shape2_2;
       msh[2,3]:=shape2_3;
       msh[2,4]:=shape2_4;
       msh[2,5]:=shape2_5;
       msh[3,1]:=shape3_1;
       msh[3,2]:=shape3_2;
       msh[3,3]:=shape3_3;
       msh[3,4]:=shape3_4;
       msh[3,5]:=shape3_5;
       msh[4,1]:=shape4_1;
       msh[4,2]:=shape4_2;
       msh[4,3]:=shape4_3;
       msh[4,4]:=shape4_4;
       msh[4,5]:=shape4_5;
       msh[5,1]:=shape5_1;
       msh[5,2]:=shape5_2;
       msh[5,3]:=shape5_3;
       msh[5,4]:=shape5_4;
       msh[5,5]:=shape5_5;
     end
     else

     begin
       label1.Caption:=m3[1,1,1].tostring+i.tostring+j.tostring;
       i:=5;
       j:=5;
       case snakedirection of



       'left': if (headpos[1]-1>=1) and (m3[headpos[1]-1,headpos[2],1]<=0) then
       begin
         headpos[1]:=headpos[1]-1;
         if m3[headpos[1],headpos[2],1]=-1 then
         begin
         snakelength:= snakelength+1;
         k:=1;

          while k=1 do begin
          i:=random(5)+1;
          j:=random(5)+1;
          if m3[i,j,1]=0 then
          begin
          m3[i,j,1]:=-1;
          k:=0;
          end;
          end;
         end;
         m3[headpos[1],headpos[2],1]:=snakelength;
       end
       else
       begin
         label3.Visible:=true;
         game_over(label1,timer1);

       end;




       'right': if (headpos[1]+1<=5) and (m3[headpos[1]+1,headpos[2],1]<=0) then
       begin
         headpos[1]:=headpos[1]+1;
         if m3[headpos[1],headpos[2],1]=-1 then
         begin
         snakelength:= snakelength+1;
         k:=1;

          while k=1 do begin
          i:=random(5)+1;
          j:=random(5)+1;
          if m3[i,j,1]=0 then
          begin
          m3[i,j,1]:=-1;
          k:=0;
          end;
          end;
         end;
         m3[headpos[1],headpos[2],1]:=snakelength;
       end
       else
       begin
         label3.Visible:=true;
         game_over(label1,timer1);
       end;




       'up':if (headpos[2]-1>=1) and (m3[headpos[1],headpos[2]-1,1]<=0) then
       begin
         headpos[2]:=headpos[2]-1;
         if m3[headpos[1],headpos[2],1]=-1 then
         begin
         snakelength:= snakelength+1;
         k:=1;

          while k=1 do begin
          i:=random(5)+1;
          j:=random(5)+1;
          if m3[i,j,1]=0 then
          begin
          m3[i,j,1]:=-1;
          k:=0;
          end;
          end;
         end;
         m3[headpos[1],headpos[2],1]:=snakelength;
       end
       else
       begin
         label3.Visible:=true;
         game_over(label1,timer1);
       end;




       'down': if (headpos[2]+1<=5) and (m3[headpos[1],headpos[2]+1,1]<=0) then
       begin
         headpos[2]:=headpos[2]+1;
         if m3[headpos[1],headpos[2],1]=-1 then
         begin
         snakelength:= snakelength+1;
         k:=1;

          while k=1 do begin
          i:=random(5)+1;
          j:=random(5)+1;
          if m3[i,j,1]=0 then
          begin
          m3[i,j,1]:=-1;
          k:=0;
          end;
          end;
         end;
         m3[headpos[1],headpos[2],1]:=snakelength;
       end
       else
       begin
         label3.Visible:=true;
         game_over(label1,timer1);
       end;

       end;


       i:=5;
       while i>0 do begin
         j:=5;
         while j>0 do begin
            if m3[i,j,1]=0 then
          msh[i,j].brush.color:=clwhite
          else if m3[i,j,1]>0 then
          begin
          msh[i,j].brush.color:=clblack;
          m3[i,j,1]:=m3[i,j,1]-1;
          end
          else
          msh[i,j].brush.color:=clnavy;

            j:=j-1;
         end;
         i:=i-1;
       end;

     end;



end;

procedure TForm1.StaticText1Click(Sender: TObject);
begin
   StaticText1.caption:=m[1,1].tostring;
end;

procedure TForm1.Shape1_1ChangeBounds(Sender: TObject);
begin

end;

procedure TForm1.Button4Click(Sender: TObject);
begin
    snakedirection:='down';
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  initiator1:=0;
  k:=5;
  label3.visible:=false;
  timer1.enabled:=true;
end;

procedure TForm1.Button5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

end;

procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
  );
begin
  case key of
  VK_DOWN:snakedirection:='down'  ;
  VK_up:snakedirection:='up';
  VK_right:snakedirection:='right';
  VK_left:snakedirection:='left';
  end;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
  );
begin
    {case key of
  VK_DOWN:snakedirection:='down'  ;
  VK_up:snakedirection:='up';
  VK_right:snakedirection:='right';
  VK_left:snakedirection:='left';
    end;}
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   snakedirection:='up';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  snakedirection:='left';
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  snakedirection:='right';
end;






end.

